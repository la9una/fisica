# Fórmulas físicas 
Listado de fórmulas físicas según temática. 

## Cinemática
Estudio del movimiento

### MRU

$$
\overrightarrow{\Delta r} =  \overrightarrow{r}_f - \overrightarrow{r}_i
$$

$$
v =  \frac{d}{t}
$$

$$
\overrightarrow{v} =  \frac{\overrightarrow{\Delta r}}{\Delta t} = \frac{\overrightarrow{r}_f - \overrightarrow{r}_i}{t_f - t_i}
$$

$$
\overrightarrow{r_f} = \overrightarrow{r_i} + \overrightarrow{v} \cdot t
$$

### MRUV

$$
\overrightarrow{r_f} =  \overrightarrow{r}_i + \overrightarrow{v}_i \cdot t + \frac{1}{2} \cdot \overrightarrow{a} \cdot t^2
$$



$$
\overrightarrow{v^2_f} =  \overrightarrow{v^2_i} + 2 \cdot \overrightarrow{a} \cdot \overrightarrow{\Delta r}
$$

$$
\overrightarrow{v_f} =  \overrightarrow{v_i} + \overrightarrow{a} \cdot t
$$

$$
\overrightarrow{v_m} = \frac {\overrightarrow{v_f} + \overrightarrow{v_i}}{2}
$$

$$
\overrightarrow{a} =  \frac{\overrightarrow{\Delta v}}{\Delta t} = \frac{\overrightarrow{v}_f - \overrightarrow{v}_i}{t_f - t_i}
$$
