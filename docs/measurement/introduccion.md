#Errores experimentales 

En más de una ocasión, nos econtraremos ante la situación de tener que **realizar mediciones** de distinto tipo. Dendiendo de magnitud, al realizar la medición obtenemos un valor determinado, por ejemplo: 0,583 m ó 1,725 gr, etc. Sin embargo, si repitiéramos varias veces el proceso de medición caeríamos en cuenta que no todas las veces obtendremos el mismo valor. Una veces será mayor, otras, menor y en otras se repetirá el obtenido la primera vez. 

**En toda medición se producen siempre errores** que el experimentador debe saber manejar. Los errores pueden clasificarse en:

1. **Errores sistemáticos**: son los provocados por los defectos en la escala del aparato empleado o manera de construcción del mismo. 
2. **Errores de apreciación**: son los que se originan por malas lecturas realizadas por el observador. La menor o mayor experiencia que posea el observador en realizar esta tarea, será la mejor o menor calidad de la medición realizada. 
3. **Errores casuales**: son originados por factores no previsibles como temperatura, presión, movimiento del soporte que sostiene el aparato, fatiga del observador, etc. 

$$
\\
$$

## Valor probable

Se trata de la media aritmética (promedio) de los valores obtenidos con las mediciones realizadas. También se conoce como "valor real":  

$$
\overline{x} = \frac { x_{1} + x_{2} + x_{3} + ... + x_{n}}{n}
$$

!!! Ejemplo
	Se desea determinar la longitud de un objeto. Para ello, se realizan 10 mediciones las cuales arrojan los siguientes valores: 

	$x_1=10,40 mm$ 

	$x_2=10,30 mm$ 
	
	$x_3=10,30 mm$ 

	$x_4=10,30 mm$ 
	
	$x_5=10,40 mm$ 

	$x_6=10,40 mm$
	
	$x_7=10,40 mm$ 

	$x_8=10,30 mm$ 
	
	$x_9=10,40 mm$
	
	$x_{10}=10,40 mm$

	El **valor probable ($\overline{x}$)** se calcula de la siguiente manera: 
	
	$$
	\overline{x} =  \frac{ 10,40 + 10,30 + 10,30 + 10,30 + 10,40 + 10,40 + 10,40 + 10,30 + 10,40 + 10,40 }{10}
	$$

	Resolviendo: 
	
	$$
	\overline{x} = 10,36 mm
	$$