## Gráficas MRU

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Gráfico de posición en función del tiempo para MRU",
  "data": {
    "values": [
      {"time": 0, "position": 0},
      {"time": 1, "position": 10},
      {"time": 2, "position": 20},
      {"time": 3, "position": 30},
      {"time": 4, "position": 40}
    ]
  },
  "encoding": {
    "x": {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
    "y": {"field": "position", "type": "quantitative", "title": "Posición (m)"},
    "tooltip": [
      {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
      {"field": "position", "type": "quantitative", "title": "Posición (m)"}
    ]
  },
  "layer": [
    {"mark": "line"},
    {"mark": {"type": "point", "filled": true, "size": 50}}
  ],
  "title": "Gráfico de posición en función del tiempo - MRU"
}
```

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Gráfico de velocidad en función del tiempo para MRU",
  "data": {
    "values": [
      {"time": 0, "velocity": 5},
      {"time": 1, "velocity": 5},
      {"time": 2, "velocity": 5},
      {"time": 3, "velocity": 5},
      {"time": 4, "velocity": 5}
    ]
  },
  "encoding": {
    "x": {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
    "y": {"field": "velocity", "type": "quantitative", "title": "Velocidad (m/s)", "scale": {"domain": [-10, 10]}},
    "tooltip": [
      {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
      {"field": "velocity", "type": "quantitative", "title": "Velocidad (m/s)"}
    ]
  },
  "layer": [
    {"mark": "line"},
    {"mark": {"type": "point", "filled": true, "size": 50}}
  ],
  "title": "Gráfico de velocidad en función del tiempo - MRU"
}
```

## Intervalos

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "data": {
    "values": [
      {"intervalo": "A", "tiempo": 0, "posicion": -3},
      {"intervalo": "A", "tiempo": 4, "posicion": 3},
      {"intervalo": "B", "tiempo": 4, "posicion": 3},
      {"intervalo": "B", "tiempo": 8, "posicion": 7},
      {"intervalo": "C", "tiempo": 8, "posicion": 7},
      {"intervalo": "C", "tiempo": 12, "posicion": 0}
    ]
  },
  "title": "Gráfico de posición en función del tiempo - MRU - Intevalos",
  "encoding": {
    "x": {"field": "tiempo", "type": "quantitative", "title": "Tiempo (s)"},
    "y": {"field": "posicion", "type": "quantitative", "title": "Posición (m)"},
    "color": {"field": "intervalo", "type": "nominal", "title": "Intervalo"},
    "tooltip": [
      {"field": "tiempo", "type": "quantitative", "title": "Tiempo (s)"},
      {"field": "posicion", "type": "quantitative", "title": "Posición (m)"}
    ]
  },
  "layer": [
    {"mark": {"type": "line", "interpolate": "linear"}},
    {"mark": {"type": "point", "filled": true, "size": 50}},
    {
      "mark": {
        "type": "text",
        "align": "left",
        "baseline": "middle",
        "dx": 10,
        "fontSize": 12,
        "fontWeight": "bold"
      }
    }
  ],
  "config": {
    "axisY": {
      "scale": {"zero": false},
      "tickCount": 6
    }
  }
}
```

  ```vegalite
  {
    "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
    "data": {
      "values": [
        {"intervalo": "A", "tiempo": 0, "posicion": -3, "velocidad": 1.5},
        {"intervalo": "A", "tiempo": 4, "posicion": 3, "velocidad": 1.5},
        {"intervalo": "B", "tiempo": 4, "posicion": 3, "velocidad": 2},
        {"intervalo": "B", "tiempo": 8, "posicion": 7, "velocidad": 2},
        {"intervalo": "C", "tiempo": 8, "posicion": 7, "velocidad": -1.75},
        {"intervalo": "C", "tiempo": 12, "posicion": 0, "velocidad": -1.75}
      ]
    },
    "title": "Gráfico de velocidad en función del tiempo",
    "encoding": {
      "x": {"field": "tiempo", "type": "quantitative", "title": "Tiempo (s)"},
      "y": {"field": "velocidad", "type": "quantitative", "title": "Velocidad (m/s)"},
      "color": {"field": "intervalo", "type": "nominal", "title": "Intervalo"},
      "tooltip": [
        {"field": "tiempo", "type": "quantitative", "title": "Tiempo (s)"},
        {"field": "velocidad", "type": "quantitative", "title": "Velocidad (m/s)"}
      ]
    },
    "layer": [
      {"mark": {"type": "line", "interpolate": "step-after"}},
      {"mark": {"type": "point", "filled": true, "size": 50}},
      {"mark": {"type": "text", "align": "left", "baseline": "middle", "dx": 10, "fontSize": 12, "fontWeight": "bold"}}
    ],
    "config": {
      "axisY": {
        "scale": {"domain": [-2, 2]},
        "tickCount": 5
      }
    }
  }

  ```
