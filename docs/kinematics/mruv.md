## Gráficas MRUV

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Gráfico de posición en función del tiempo para MRUV",
  "data": {
    "values": [
      {"time": 0, "position": 0},
      {"time": 1, "position": 5},
      {"time": 2, "position": 20},
      {"time": 3, "position": 45},
      {"time": 4, "position": 80}
    ]
  },
  "encoding": {
    "x": {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
    "y": {"field": "position", "type": "quantitative", "title": "Posición (m)"},
    "tooltip": [
      {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
      {"field": "position", "type": "quantitative", "title": "Posición (m)"}
    ]
  },
  "layer": [
    {"mark": {"type": "line", "interpolate": "monotone"}},
    {"mark": {"type": "point", "filled": true, "size": 50}}
  ],
  "title": "Gráfico de posición en función del tiempo para MRUV"
}
```

```vegalite
{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "Gráfico de velocidad en función del tiempo para MRUV",
  "data": {
    "values": [
      {"time": 0, "velocity": 0},
      {"time": 1, "velocity": 5},
      {"time": 2, "velocity": 10},
      {"time": 3, "velocity": 15},
      {"time": 4, "velocity": 20}
    ]
  },
  "encoding": {
    "x": {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
    "y": {"field": "velocity", "type": "quantitative", "title": "Velocidad (m/s)"},
    "tooltip": [
      {"field": "time", "type": "quantitative", "title": "Tiempo (s)"},
      {"field": "velocity", "type": "quantitative", "title": "Velocidad (m/s)"}
    ]
  },
  "layer": [
    {"mark": "line"},
    {"mark": {"type": "point", "filled": true, "size": 50}}
  ],
  "title": "Gráfico de velocidad en función del tiempo - MRUV"
}

```
